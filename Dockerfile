FROM node:18-alpine3.16

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

RUN npm install -g @angular/cli@16.1.4

EXPOSE 4200

ENTRYPOINT ["ng", "serve", "--host", "0.0.0.0", "--disable-host-check"]

# CMD ["npm", "start"]



#ENTRYPOINT ["ng", "serve", "--host", "0.0.0.0", "--disable-host-check"]
# RUN npm i -g npm@9.6.4
# RUN npm install -g @angular/cli@16.1.4


# RUN ng new ccs-frontend

# WORKDIR /ccs-frontend/
# RUN ng update
# COPY . ccs-frontend/


# RUN ng add @ng-bootstrap/ng-bootstrap --defaults --skip-confirmation

# ENTRYPOINT ["ng", "serve", "--host", "0.0.0.0", "--disable-host-check"]