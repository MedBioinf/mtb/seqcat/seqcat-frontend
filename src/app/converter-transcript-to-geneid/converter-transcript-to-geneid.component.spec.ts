import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterTranscriptToGeneidComponent } from './converter-transcript-to-geneid.component';

describe('ConverterTranscriptToGeneidComponent', () => {
  let component: ConverterTranscriptToGeneidComponent;
  let fixture: ComponentFixture<ConverterTranscriptToGeneidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterTranscriptToGeneidComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterTranscriptToGeneidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
