import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-transcript-to-geneid',
  templateUrl: './converter-transcript-to-geneid.component.html',
  styleUrls: ['./converter-transcript-to-geneid.component.css']
})
export class ConverterTranscriptToGeneidComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/transcriptToGeneId/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);

    var inputName: string = this.ccsForm.value.inputName;

    var req: string = this.buildRequest(inputName);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string) {
    var request: string = this.requestSrc + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['NM_201628.3,NM_201628,ENST00000311936,NM_000546,NM_002524.4']

    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;

    this.ccsForm.patchValue({
      inputName: example,
    });
  }
}

class Data {
  name: string;
  ensembl_mane: string;
  hgnc: string;
  hgnc_id: string;
  ncbi_mane: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, ensembl_mane: string, hgnc: string, hgnc_id: string, ncbi_mane: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.ensembl_mane = ensembl_mane;
    this.hgnc = hgnc;
    this.hgnc_id = hgnc_id;
    this.ncbi_mane = ncbi_mane;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['ensembl_mane'],
            j_element['data']['hgnc'],
            j_element['data']['hgnc_id'],
            j_element['data']['ncbi_mane']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          "",
          "",
          "",
        ""));
        }
      }
    }
  }
}

