import { Component } from '@angular/core';

@Component({
  selector: 'app-helping',
  templateUrl: './helping.component.html',
  styleUrls: ['./helping.component.css']
})
export class HelpingComponent {
  show_help = ""

  scrollTo(): void {
    setTimeout(() => (document.getElementById('helpsection') as HTMLElement).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"}), 20);
  }
}