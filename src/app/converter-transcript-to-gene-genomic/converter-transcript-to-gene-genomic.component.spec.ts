import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterTranscriptToGeneGenomicComponent } from './converter-transcript-to-gene-genomic.component';

describe('ConverterTranscriptToGeneGenomicComponent', () => {
  let component: ConverterTranscriptToGeneGenomicComponent;
  let fixture: ComponentFixture<ConverterTranscriptToGeneGenomicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterTranscriptToGeneGenomicComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterTranscriptToGeneGenomicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
