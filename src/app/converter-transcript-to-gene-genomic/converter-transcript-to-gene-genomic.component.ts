import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-transcript-to-gene-genomic',
  templateUrl: './converter-transcript-to-gene-genomic.component.html',
  styleUrls: ['./converter-transcript-to-gene-genomic.component.css']
})
export class ConverterTranscriptToGeneGenomicComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/cdnaToGene/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['AKT1:c.G49A,KMT2A:c.G8819C,KMT2A:8819G>C']
    var references: Array<string> = ['hg38']

    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Data {
  name: string;
  chromosome: string;
  gene_name: string;
  parsed_data: string;
  transcript: string;
  validation: string;
  variant_exchange: string;
  variant_exchange_long: string;
  variant_g: string;
  variant_p: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, chromosome: string, gene_name: string, parsed_data: string, transcript: string, validation: string, variant_exchange: string, variant_exchange_long: string, variant_g: string, variant_p: string) {
    this.response = response;
    this.name = name;
    this.error = error;
    this.chromosome = chromosome;
    this.gene_name = gene_name;
    this.parsed_data = parsed_data;
    this.transcript = transcript; 
    this.validation = validation;
    this.variant_exchange = variant_exchange;
    this.variant_exchange_long = variant_exchange_long;
    this.variant_g = variant_g;
    this.variant_p = variant_p;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['chromosome'],
            j_element['data']['gene_name'],
            j_element['data']['parsed_data'],
            j_element['data']['transcript'],
            j_element['data']['validation'],
            j_element['data']['variant_exchange'],
            j_element['data']['variant_exchange_long'],
            j_element['data']['variant_g'],
            j_element['data']['variant_p']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",
          "",));
        }
      }
    }
  }
}

