import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputFormViewComponent } from './input-form-view.component';

describe('InputFormViewComponent', () => {
  let component: InputFormViewComponent;
  let fixture: ComponentFixture<InputFormViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputFormViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InputFormViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
