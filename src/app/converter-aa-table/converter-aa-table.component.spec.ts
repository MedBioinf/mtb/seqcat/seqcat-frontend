import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterAaTableComponent } from './converter-aa-table.component';

describe('ConverterAaTableComponent', () => {
  let component: ConverterAaTableComponent;
  let fixture: ComponentFixture<ConverterAaTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterAaTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterAaTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
