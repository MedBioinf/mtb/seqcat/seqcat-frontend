import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterGeneNormalizerComponent } from './converter-gene-normalizer.component';

describe('ConverterGeneNormalizerComponent', () => {
  let component: ConverterGeneNormalizerComponent;
  let fixture: ComponentFixture<ConverterGeneNormalizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterGeneNormalizerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterGeneNormalizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
