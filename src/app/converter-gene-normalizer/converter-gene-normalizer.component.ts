import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-gene-normalizer',
  templateUrl: './converter-gene-normalizer.component.html',
  styleUrls: ['./converter-gene-normalizer.component.css']
})
export class ConverterGeneNormalizerComponent {

  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/GeneNormalizer';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);

    var inputName: string = this.ccsForm.value.inputName;

    var req: string = this.buildRequest(inputName);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string) {
    var request: string = this.requestSrc + "/"
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['TP53,p53,PUM,UGA']

    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;

    this.ccsForm.patchValue({
      inputName: example,
    });
  }
}

class Data {
  is_approved: string;
  approved_symbol: string;
  approved_name: string;
  chromosome: string;
  hgnc_id: string;
  previous_symbols: Array<string>;
  alias_symbols: Array<string>;

  name: string;
  response: string;
  error: string;


  constructor(name: string, response: string, error: string,
    is_approved: string,
    approved_symbol: string,
    approved_name: string,
    chromosome: string,
    hgnc_id: string,
    previous_symbols: Array<string>,
    alias_symbols: Array<string>) {

    this.is_approved = is_approved;
    this.approved_symbol = approved_symbol;
    this.approved_name = approved_name;
    this.chromosome = chromosome;
    this.hgnc_id = hgnc_id;
    this.previous_symbols = previous_symbols;
    this.alias_symbols = alias_symbols;

    this.response = response
    this.name = name;
    this.error = error;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {
          this.data.push(
            new Data(
              j_element['header']['qid'],
              j_element['header']['status_code'],
              "",
              j_element['data']['is_approved'],
              j_element['data']['approved_symbol'],
              j_element['data']['approved_name'],
              j_element['data']['chromosome'],
              j_element['data']['hgnc_id'],
              j_element['data']['previous_symbols'],
              j_element['data']['alias_symbols']));
          this.number_elements += 1;
        }
        else {
          this.data.push(
            new Data(
              j_element['header']['qid'],
              j_element['header']['status_code'],
              j_element['header']['error_message'],
              "", "", "", "", "", [], []));
        }
      }
    }
  }
}

