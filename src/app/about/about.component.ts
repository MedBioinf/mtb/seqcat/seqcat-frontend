import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent {

  document_text = "Kornrumpf K, Kurz NS, Drofenik K, et al. SeqCAT: Sequence Conversion and Analysis Toolbox. Nucleic Acids Res. 2024;52(W1):W116-W120. doi:10.1093/nar/gkae422"

  myFunction() {
    // Copy the text inside the text field
    navigator.clipboard.writeText(this.document_text);

    // Alert the copied text
    alert('Copied the text: ' + this.document_text);
  }
}
