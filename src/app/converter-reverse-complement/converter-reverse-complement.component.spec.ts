import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterReverseComplementComponent } from './converter-reverse-complement.component';

describe('ConverterReverseComplementComponent', () => {
  let component: ConverterReverseComplementComponent;
  let fixture: ComponentFixture<ConverterReverseComplementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterReverseComplementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterReverseComplementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
