import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-reverse-complement',
  templateUrl: './converter-reverse-complement.component.html',
  styleUrls: ['./converter-reverse-complement.component.css']
})
export class ConverterReverseComplementComponent {
  ccsForm: FormGroup;

  r_complement: ReverseComplement = new ReverseComplement();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/ReverseComplement/';

  complete_request: string = '';

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      conversion_type: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.conversion_type);

    var inputName: string = this.ccsForm.value.inputName;
    var conversion_type: string = this.ccsForm.value.conversion_type;

    var req: string = this.buildRequest(inputName, conversion_type);
    this.complete_request = req;
    this.reqObj = new ReverseComplement()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.r_complement = new ReverseComplement();
  }

  buildRequest(inputName: string, conversion_type: string) {
    var request: string = this.requestSrc + '/' + conversion_type + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['ACTGtcgaXXYNNNNnnnn,ACTGtcgNNNnnnn,ACTgGctttaXY,GGCGAGAGAATCTTCTTTCTGTCTATCGAAGAATGGGCATGGGGTGGCAACCGTCATGCTAGCGTGCGGGGTGCACTTGGTAACCATTTGGGACACCGGACACTCGCTGTTTTCGAAATTACCCTTTAAGCGCGGGTATTGAACCAGGCTTATGCCCAGCATCGTTGCAAGCAGACTCAAACTAGATATATTATGCCCGCCATACAGACGAAACTAGTCGGAGATTATCGAGCATACTATCACGTCGGCGACCACTAGTGAGTTACTAGAGCCGAGGGGCAACGTTGATGCCCCTAAGAA']
    var references: Array<string> = ['reverse_complement']


    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      conversion_type: reference,
    });
  }
}

class Data {
  name: string;
  response: string;
  error: string;
  rev_comp: string;

  constructor(name: string, response: string, error: string, rev_comp:string) {
    this.name = name;
    this.response = response
    this.error = error;
    this.rev_comp = rev_comp;
  }
}

class ReverseComplement {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {
          this.data.push(new Data(
            j_element['header']['qid'].slice(0,15) + '...',
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            ""));
        }
      }
    }
  }
}