import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterBlossumMatrixScoreCalculatorComponent } from './converter-blossum-matrix-score-calculator.component';

describe('ConverterBlossumMatrixScoreCalculatorComponent', () => {
  let component: ConverterBlossumMatrixScoreCalculatorComponent;
  let fixture: ComponentFixture<ConverterBlossumMatrixScoreCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterBlossumMatrixScoreCalculatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterBlossumMatrixScoreCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
