import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-protein-info',
  templateUrl: './converter-protein-info.component.html',
  styleUrls: ['./converter-protein-info.component.css']
})
export class ConverterProteinInfoComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/get_protein_sequence/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);

    var inputName: string = this.ccsForm.value.inputName;

    var req: string = this.buildRequest(inputName);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string) {
    var request: string = this.requestSrc + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['NM_005228.5,TP53,BRAF,KRAS']

    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;

    this.ccsForm.patchValue({
      inputName: example,
    });
  }
}

class Data {
  gene_name: string;
  protein_id: string;
  protein_sequence: string;
  transcript: string;
  variant: string;

  name: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, gene_name: string, protein_id: string,
    protein_sequence: string, transcript: string, variant: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.gene_name = gene_name;
    this.protein_id = protein_id;
    this.protein_sequence = protein_sequence;
    this.transcript = transcript;
    this.variant = variant;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      console.log(j_element);
      let keys = Object.keys(j_element);
      for (let key of keys) {
        if (j_element[key]['header'] != null) {
          if (j_element[key]['header']['status_code'] == 200) {
            this.data.push(
              new Data(
                j_element[key]['header']['qid'],
                j_element[key]['header']['status_code'],
                j_element[key]['header']['error_message'],
                j_element[key]['data']['gene_name'],
                j_element[key]['data']['protein_id'],
                j_element[key]['data']['protein_sequence'],
                j_element[key]['data']['transcript'],
                j_element[key]['data']['variant']));
            this.number_elements += 1;
          }
          else {
            this.data.push(
              new Data(
                j_element[key]['header']['qid'],
                j_element[key]['header']['status_code'],
                j_element[key]['header']['error_message'],
                "",
                "",
                "",
                "",
                ""));
          }
        }
      }
    }
  }
}