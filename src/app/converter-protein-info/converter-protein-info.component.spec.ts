import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterProteinInfoComponent } from './converter-protein-info.component';

describe('ConverterProteinInfoComponent', () => {
  let component: ConverterProteinInfoComponent;
  let fixture: ComponentFixture<ConverterProteinInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterProteinInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterProteinInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
