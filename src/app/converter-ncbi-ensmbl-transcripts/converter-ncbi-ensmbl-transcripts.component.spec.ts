import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterNcbiEnsmblTranscriptsComponent } from './converter-ncbi-ensmbl-transcripts.component';

describe('ConverterNcbiEnsmblTranscriptsComponent', () => {
  let component: ConverterNcbiEnsmblTranscriptsComponent;
  let fixture: ComponentFixture<ConverterNcbiEnsmblTranscriptsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterNcbiEnsmblTranscriptsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterNcbiEnsmblTranscriptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
