import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterProteinDnaComponent } from './converter-protein-dna.component';

describe('ConverterProteinDnaComponent', () => {
  let component: ConverterProteinDnaComponent;
  let fixture: ComponentFixture<ConverterProteinDnaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterProteinDnaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterProteinDnaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
