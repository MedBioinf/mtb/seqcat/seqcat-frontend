import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-protein-dna',
  templateUrl: './converter-protein-dna.component.html',
  styleUrls: ['./converter-protein-dna.component.css']
})
export class ConverterProteinDnaComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();
  reqVCF: VCF = new VCF();

  random_iterator: number = 0

  reqObj: any;
  reqObjVcf: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/GeneToGenomic/';
  requestSrcVCF: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/GeneToGenomicVCF/';

  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);
    this.complete_request = req;
    var reqVCF : string = this.buildRequestVCF(inputName, referenceGenome);
    this.reqObj = new LiftOver()
    this.reqObjVcf = new VCF()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });

    this.http.get(reqVCF).subscribe((json: any) => {
      console.log(json);
      this.reqObjVcf.fill_data(json);
      console.log(this.reqObjVcf);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
    this.reqVCF = new VCF();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  buildRequestVCF(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrcVCF + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['CSPG4:F821S,APOB:N3411K,MECOM:P75T,RSPH3:I242R,KIF13A:N887D,TP53:R282W,BRAF:V600E,DCHS1:H3004Y']
    var references: Array<string> = ['hg38']


    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Data {
  nucleotide_change: string;
  chromosome: string;
  results_string: string;
  transcript_id: string;
  c_dna_string: string;
  name: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, nucleotide_change: string, chromosome: string,
    results_string: string, transcript_id:string, c_dna_string: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.nucleotide_change = nucleotide_change;
    this.chromosome = chromosome;
    this.results_string = results_string;
    this.transcript_id = transcript_id;
    this.c_dna_string = c_dna_string;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data'][0]['nucleotide_change'],
            j_element['data'][0]['chromosome'],
            j_element['data'][0]['results_string'],
            j_element['data'][0]['transcript_id'],
            j_element['data'][0]['c_dna_string']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          "",
          "",
          "",
          "",
          ""));
        }
      }
    }
  }
}

class VCF {

  data: string = "";
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    this.data = json['vcf'];
    this.data.replace('\n', '<br>')
  }
}