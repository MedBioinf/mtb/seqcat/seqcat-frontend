import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-genom-to-protein-sequence',
  templateUrl: './converter-genom-to-protein-sequence.component.html',
  styleUrls: ['./converter-genom-to-protein-sequence.component.css']
})
export class ConverterGenomToProteinSequenceComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/ConvertGenomicSequence/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);

    var inputName: string = this.ccsForm.value.inputName;

    var req: string = this.buildRequest(inputName);
    this.complete_request = req;

    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string) {
    var request: string = this.requestSrc + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['ATGTTGGAGATCTGCCTGAAGCTGGTGGGCTGCAAATCCAAGAAGGGGXTGTCCTCGTCCTCCAGCTGTTATCTGGAAGC,CCTTCATCCCTATCAGAGCTTGGAGCCAATGATCAGGGTTATTCCCTTGGGACAGACTTCCTACTCACAGTCGGTCACATTGGGCTACTCCATGGGTCT']

    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;

    this.ccsForm.patchValue({
      inputName: example,
    });
  }
}

class Data {
  one_letter_seq: string;
  three_letter_seq: string;
  warning: string;

  name: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, one_letter_seq: string, three_letter_seq: string, warning:string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.one_letter_seq = one_letter_seq;
    this.three_letter_seq = three_letter_seq;
    this.warning = warning;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      console.log(j_element);
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {
          this.data.push(
            new Data(
              j_element['header']['qid'].slice(0, 15) + '...',
              j_element['header']['status_code'],
              j_element['header']['error_message'],
              j_element['data']['aa_sequence']['one_letter_seq'],
              j_element['data']['aa_sequence']['three_letter_seq'],
              j_element['data']['info']['warning'].join(';\n'),));
          this.number_elements += 1;
        }
        else {
          this.data.push(
            new Data(
              j_element['header']['qid'].slice(0, 15) + '...',
              j_element['header']['status_code'],
              j_element['header']['error_message'],
              "",
              "",
              "",
            ));
        }
      }
    }
  }
}
