import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterGenomToProteinSequenceComponent } from './converter-genom-to-protein-sequence.component';

describe('ConverterGenomToProteinSequenceComponent', () => {
  let component: ConverterGenomToProteinSequenceComponent;
  let fixture: ComponentFixture<ConverterGenomToProteinSequenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterGenomToProteinSequenceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterGenomToProteinSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
