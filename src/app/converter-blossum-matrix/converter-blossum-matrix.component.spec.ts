import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterBlossumMatrixComponent } from './converter-blossum-matrix.component';

describe('ConverterBlossumMatrixComponent', () => {
  let component: ConverterBlossumMatrixComponent;
  let fixture: ComponentFixture<ConverterBlossumMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterBlossumMatrixComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterBlossumMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
