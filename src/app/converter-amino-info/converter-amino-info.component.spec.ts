import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterAminoInfoComponent } from './converter-amino-info.component';

describe('ConverterAminoInfoComponent', () => {
  let component: ConverterAminoInfoComponent;
  let fixture: ComponentFixture<ConverterAminoInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterAminoInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterAminoInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
