import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-amino-info',
  templateUrl: './converter-amino-info.component.html',
  styleUrls: ['./converter-amino-info.component.css']
})
export class ConverterAminoInfoComponent {
  reqObj: any;

  requestInfo: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/aminoacidInfo/';

  constructor(private http: HttpClient) {
    this.getAminoacidInfo()
  }

  getAminoacidInfo() {
    this.reqObj = new AminoacidInfo()
    this.http.get(this.requestInfo).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }
}


class Data {

  one_letter?: string;
  three_letter?: string;
  aminoacid?: string;
  atoms?: number;
  charge?: number;
  hydropathy?: number;
  hydropathy_index?: number;
  polarity?: string;
  mass?: number;
  rsa?: number;
  chemical_class?: string;
  sum_formula?: string;
  volume?: number;
  bg_color?: string;
  image_path: string;
  wiki_link: string;


  constructor(one_letter?: string, three_letter?: string,
    aminoacid?: string, atoms?: number, charge?: number, hydropathy?: number, hydropathy_index?: number, polarity?: string, mass?: number,
    rsa?: number, chemical_class?: string, sum_formula?: string, volume?: number, bg_color?: string) {
    this.one_letter = one_letter;
    this.three_letter = three_letter;
    this.aminoacid = aminoacid;
    this.atoms = atoms;
    this.charge = charge;
    this.hydropathy = hydropathy;
    this.hydropathy_index = hydropathy_index;
    this.mass = mass;
    this.rsa = rsa;
    this.chemical_class = chemical_class;
    this.sum_formula = sum_formula;
    this.volume = volume;
    this.polarity = polarity;

    this.bg_color = bg_color;

    this.image_path = "./assets/images/aa_structures/" + aminoacid + ".svg";
    this.wiki_link = "https://en.wikipedia.org/wiki/" + aminoacid;
  }
}

class AminoacidInfo {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      console.log("No data");
      return;
    }
    for (let j_element of json) {
      this.data.push(new Data(
        j_element['1-Letter'],
        j_element['3-Letter'],
        j_element['AminoAcid'],
        j_element['Atoms'],
        j_element['Charge'],
        j_element['Hydropathy'],
        j_element['Hydropathy index'],
        j_element['Polarity'],
        j_element['Mass'],
        j_element['RSA'],
        j_element['Chemical'],
        j_element['SumFormula'],
        j_element['Volume'],
        j_element['Color'],
      ));
      this.number_elements += 1;
    }
  }
}
