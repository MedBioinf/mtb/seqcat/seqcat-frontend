import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-exon-info',
  templateUrl: './converter-exon-info.component.html',
  styleUrls: ['./converter-exon-info.component.css']
})

export class ConverterExonInfoComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/ExonInfo/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['KRAS,BRAF,TP53']
    var references: Array<string> = ['hg38']

    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Exon {
  alt_end_i: string;
  alt_start_i: string;
  cds_end: string;
  cds_length: string;
  cds_start: string;
  chr: string;
  cummulative_length: string;
  exon: string;
  g_seq: string;
  gene: string;
  strand: string;
  transcript: string;
  tx_end_i: string;
  tx_start_i: string;

  constructor(alt_end_i: string, alt_start_i: string, cds_end: string, cds_length: string, cds_start: string, chr: string, cummulative_length: string,
    exon: string, g_seq: string, gene: string, strand: string, transcript: string, tx_end_i: string, tx_start_i: string) {
    this.alt_end_i = alt_end_i;
    this.alt_start_i = alt_start_i;
    this.cds_end = cds_end;
    this.cds_length = cds_length;
    this.cds_start = cds_start;
    this.chr = chr;
    this.cummulative_length = cummulative_length;
    this.exon = exon;
    this.g_seq = g_seq;
    this.gene = gene;
    this.strand = strand;
    this.transcript = transcript;
    this.tx_end_i = tx_end_i;
    this.tx_start_i = tx_start_i;
  }
}

class Data {
  name: string;
  response: string;
  error: string;

  exons: Exon[];
  dna_sequence_full: string;
  protein_sequence_one_letter: string;
  protein_sequence_three_letter: string;


  constructor(name: string, response: string, error: string, exons: Exon[], dna_sequence_full: string, protein_sequence_one_letter: string,
    protein_sequence_three_letter: string) {
    this.name = name;
    this.response = response;
    this.error = error;

    this.exons = exons;
    this.dna_sequence_full = dna_sequence_full;
    this.protein_sequence_one_letter = protein_sequence_one_letter;
    this.protein_sequence_three_letter = protein_sequence_three_letter;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {

          let exons = [];
          for (let exon of j_element['data']['exons']) {
            exons.push(new Exon(exon['alt_end_i'], exon['alt_start_i'], exon['cds_end'], exon['cds_length'],
              exon['cds_start'], exon['chr'], exon['cummulative_length'], exon['exon'], exon['g_seq'],
              exon['gene'], exon['strand'], exon['transcript'], exon['tx_end_i'], exon['tx_start_i']));
          }

          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            exons,
            j_element['data']['dna_sequence_full'],
            j_element['data']['protein_sequence_one_letter'],
            j_element['data']['protein_sequence_three_letter']
          ));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            [],
            "",
            "",
            ""));
        }
      }
    }
  }
}