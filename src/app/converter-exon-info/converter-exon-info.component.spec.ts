import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterExonInfoComponent } from './converter-exon-info.component';

describe('ConverterExonInfoComponent', () => {
  let component: ConverterExonInfoComponent;
  let fixture: ComponentFixture<ConverterExonInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterExonInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterExonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
