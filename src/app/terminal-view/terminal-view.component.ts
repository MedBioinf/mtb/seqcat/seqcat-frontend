import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-terminal-view',
  templateUrl: './terminal-view.component.html',
  styleUrls: ['./terminal-view.component.css']
})
export class TerminalViewComponent {
  @Input() terminal_string: string = "";
}
