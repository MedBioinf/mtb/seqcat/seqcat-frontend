import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.css']
})
export class ConverterComponent {

  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();
  genomic: Genomic = new Genomic();
  protein: Protein = new Protein();
  proteinSequence: ProteinSequence = new ProteinSequence("");

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/';

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
      conversionType: ['', { validators: [Validators.required] }]
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);
    console.log(this.ccsForm.value.conversionType);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;
    var conversionType: string = this.ccsForm.value.conversionType;

    var req: string = this.buildRequest(inputName, referenceGenome, conversionType);
    this.reqObj = this.getSearchObj(inputName, conversionType);

    this.http.get(req).subscribe((json: any) => {
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
    this.genomic = new Genomic();
    this.protein = new Protein();
    this.proteinSequence = new ProteinSequence("");
  }

  buildRequest(inputName: string, referenceGenome: string, conversionType: string) {
    if (conversionType == 'liftover') {
      if (referenceGenome == 'hg38') {
        referenceGenome = 'hg38:hg19';
      } else {
        referenceGenome = 'hg19:hg38';
      }
    }

    if (conversionType != 'get_protein_sequence') {
      var request: string = this.requestSrc;
      request += conversionType + '/' + referenceGenome + '/' + inputName;
      console.log(request);
    }
    else {
      var request: string = this.requestSrc;
      request += conversionType + '/' + inputName;
      console.log(request);
    }

    return request;
  }

  getSearchObj(inputName: string, conversionType: string) {
    this.selReqObj = conversionType
    if (conversionType == 'liftover') {
      return new LiftOver();
    }
    else if (conversionType == 'GenomicToGene') {
      return new Genomic();
    }
    else if (conversionType == 'GeneToGenomic') {
      return new Protein();
    }
    else {
      return new ProteinSequence(inputName);
    }
  }

  getExampleData() {
    var examples: Array<string> = ['chr17:7673776', 'chr17:7673776', 'chr17:g.7673776G>A', 'JAK1:L910P', 'TP53', 'JAK1:L910P']
    var references: Array<string> = ['hg38', 'hg19', 'hg38', 'hg38', 'hg38', 'hg19']
    var conversion_types: Array<string> = ['liftover', 'liftover', 'GenomicToGene', 'GeneToGenomic', 'get_protein_sequence', 'get_protein_sequence']

    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];
    var conversion_type: string = conversion_types[this.random_iterator];

    if (this.random_iterator == examples.length - 1){
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }
      

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;
    (<HTMLInputElement>document.getElementById(conversion_type)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
      conversionType: conversion_type,
    });
  }
}

class LiftOver {
  name: string = "";
  chromosome: string = "";
  position: [] = [];
  strand: [] = [];

  fill_data(json: any) {
    if (json[0]['data'] != null) {
      this.name = json[0]['header']['qid'];
      this.chromosome = json[0]['data']['chromosome'];
      this.position = json[0]['data']['position'];
      this.strand = json[0]['data']['strand'];
    }
  }
}

class Genomic {
  name: string = "";
  gene_name: string = "";
  transcript: string = "";
  variant: string = "";
  variant_exchange: string = "";
  variant_exchange_long: string = "";

  fill_data(json: any) {
    if (json[0]['data'] != null) {
      this.name = json[0]['header']['qid'];
      this.gene_name = json[0]['data']['gene_name'];
      this.transcript = json[0]['data']['transcript'];
      this.variant = json[0]['data']['variant'];
      this.variant_exchange = json[0]['data']['variant_exchange'];
      this.variant_exchange_long = json[0]['data']['variant_exchange_long'];
    }
  }
}

class Protein {
  name: string = "";
  transcript_id: string = "";
  nucleotide_change: string = "";
  pos_start: string = "";
  pos_end: string = "";
  results_string: string = "";
  cds_start: string = "";
  cds_end: string = "";
  c_dna_string: string = "";

  fill_data(json: any) {
    console.log(json);
    if (json[0]['data'] != null) {
      this.name = json[0]['header']['qid'];
      this.transcript_id = json[0]['data']['transcript_id'];
      this.nucleotide_change = json[0]['data'][0]['nucleotide_change'];
      this.pos_start = json[0]['data'][0]['pos_start'];
      this.pos_end = json[0]['data'][0]['pos_end'];
      this.results_string = json[0]['data'][0]['results_string'];
      this.cds_start = json[0]['data'][0]['cds_start'];
      this.cds_end = json[0]['data'][0]['cds_end'];
      this.c_dna_string = json[0]['data'][0]['c_dna_string'];
    }
  }
}

class ProteinSequence {
  inputVar: string;
  name: string = "";
  gene_name: string = "";
  protein_id: string = "";
  protein_sequence: string = "";
  trancript: string = "";
  variant: string = "";

  constructor(inputVar: string) {
    this.inputVar = inputVar;
  }

  fill_data(json: any) {
    if (json[0][this.inputVar]['data'] != null) {
      this.name = json[0][this.inputVar]['header']['qid'];
      this.gene_name = json[0][this.inputVar]['data']['gene_name'];
      this.protein_id = json[0][this.inputVar]['data']['protein_id'];
      this.protein_sequence = json[0][this.inputVar]['data']['protein_sequence'];
      this.trancript = json[0][this.inputVar]['data']['trancript'];
      this.variant = json[0][this.inputVar]['data']['variant'];
    }

    this.protein_sequence = "\n" + this.protein_sequence.replace(/(.{40})/g, "$1\n");
  }
}

