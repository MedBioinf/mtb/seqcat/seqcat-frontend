import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpressumDataPrivacyComponent } from './impressum-data-privacy.component';

describe('ImpressumDataPrivacyComponent', () => {
  let component: ImpressumDataPrivacyComponent;
  let fixture: ComponentFixture<ImpressumDataPrivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImpressumDataPrivacyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImpressumDataPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
