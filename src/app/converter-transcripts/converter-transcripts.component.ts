import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-transcripts',
  templateUrl: './converter-transcripts.component.html',
  styleUrls: ['./converter-transcripts.component.css']
})
export class ConverterTranscriptsComponent {
  ccsForm: FormGroup;

  liftover: Transcript = new Transcript();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/getTranscripts/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);

    this.complete_request = req;
    this.reqObj = new Transcript()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new Transcript();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['KRAS,BRAF,TP53,NRAS']
    var references: Array<string> = ['hg38']


    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Data {
  name: string;
  response: string;
  error: string;

  ensembl: string;
  ncbi: string;
  ncbi_all: Array<string> = [];
  ensembl_all: Array<string> = [];


  constructor(name: string, response: string, error: string, ensembl: string, ncbi: string, ncbi_all: Array<string>, ensembl_all: Array<string>) {
    this.response = response
    this.name = name;
    this.error = error;
    this.ensembl = ensembl;
    this.ncbi = ncbi;
    this.ncbi_all = ncbi_all;
    this.ensembl_all = ensembl_all;
  }
}

class Transcript {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {

          // Get all ensembl transcripts
          let ensembl_all: Array<string> = j_element['data']['ensmbl_transcripts'];
          let ensembl_all_list: Array<string> = [];
          for (let ensembl_element of ensembl_all) {
            ensembl_all_list.push(ensembl_element[3]);
          }
          ensembl_all_list = [...new Set(ensembl_all_list)];

          // Get all ncbi transcripts
          let ncbi_all: Array<string> = j_element['data']['ncbi_transcripts'];
          let ncbi_all_list: Array<string> = [];
          for (let ncbi_element of ncbi_all) {
            ncbi_all_list.push(ncbi_element[3]);
          }
          ncbi_all_list = [...new Set(ncbi_all_list)];

          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['ensmbl_mane_transcript'][3],
            j_element['data']['ncbi_mane_transcript'][3],
            ncbi_all_list,
            ensembl_all_list));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            "",
            "", 
            [], 
            []));
        }
      }
    }
  }
}