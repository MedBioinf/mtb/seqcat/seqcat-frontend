import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterTranscriptsComponent } from './converter-transcripts.component';

describe('ConverterTranscriptsComponent', () => {
  let component: ConverterTranscriptsComponent;
  let fixture: ComponentFixture<ConverterTranscriptsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterTranscriptsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterTranscriptsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
