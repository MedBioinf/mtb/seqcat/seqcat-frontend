import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterGeneInKeggPathwayComponent } from './converter-gene-in-kegg-pathway.component';

describe('ConverterGeneInKeggPathwayComponent', () => {
  let component: ConverterGeneInKeggPathwayComponent;
  let fixture: ComponentFixture<ConverterGeneInKeggPathwayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterGeneInKeggPathwayComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterGeneInKeggPathwayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
