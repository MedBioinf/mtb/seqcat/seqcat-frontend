import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-gene-in-kegg-pathway',
  templateUrl: './converter-gene-in-kegg-pathway.component.html',
  styleUrls: ['./converter-gene-in-kegg-pathway.component.css']
})
export class ConverterGeneInKeggPathwayComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  innerHTML: string = '';

  reqObj: any = "";
  selReqObj: string = "";
  selected_pathway: string = ""

  // selected_genes: string = ""
  // selected_connections: string = ""

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/visualizePathway';

  kegg_pathways = [["hsa05210", "Colorectal cancer"],
  ["hsa05212", "Pancreatic cancer"],
  ["hsa05225", "Hepatocellular carcinoma"],
  ["hsa05226", "Gastric cancer"],
  ["hsa05214", "Glioma"],
  ["hsa05216", "Thyroid cancer"],
  ["hsa05221", "Acute myeloid leukemia"],
  ["hsa05220", "Chronic myeloid leukemia"],
  ["hsa05217", "Basal cell carcinoma"],
  ["hsa05218", "Melanoma"],
  ["hsa05211", "Renal cell carcinoma"],
  ["hsa05219", "Bladder cancer"],
  ["hsa05215", "Prostate cancer"],
  ["hsa05213", "Endometrial cancer"],
  ["hsa05224", "Breast cancer"],
  ["hsa05222", "Small cell lung cancer"],
  ["hsa04110", "Cell cycle"],
  ["hsa04210", "Apoptosis"],
  ["hsa04115", "p53 signaling pathway"],
  //["hsa04010", "MAPK signaling pathway"], Not working
  ["hsa04012", "ErbB signaling pathway"],
  ["hsa04014", "Ras signaling pathway"],
  ["hsa04015", "Rap1 signaling pathway"],
  //["hsa04310", "Wnt signaling pathway"], Not working
  ["hsa04330", "Notch signaling pathway"],
  ["hsa04340", "Hedgehog signaling pathway"],
  ["hsa04350", "NTGF-beta signaling pathway"],
  ["hsa04390", "Hippo signaling pathway"],
  ["hsa04370", "VEGF signaling pathway"],
  ["hsa04371", "Apelin signaling pathway"],
  ["hsa04630", "JAK-STAT signaling pathway"],
  ["hsa04064", "NF-kappa B signaling pathway"],
  ["hsa04668", "TNF signaling pathway"],
  ["hsa04066", "HIF-1 signaling pathway"],
  ["hsa04068", "FoxO signaling pathway"],
  // ["hsa04020", "Calcium signaling pathway"], Not working
  // ["hsa04070", "Phosphatidylinositol signaling system"], Not working
  ["hsa04072", "Phospholipase D signaling pathway"],
  ["hsa04071", "Sphingolipid signaling pathway"],
  ["hsa04024", "cAMP signaling pathway"],
  ["hsa04022", "cGMP-PKG signaling pathway"],
  // ["hsa04151", "PI3K-Akt signaling pathway"], not working
  ["hsa04152", "AMPK signaling pathway"],
  ["hsa04150", "mTOR signaling pathway"],
  
  //["hsa05223", "Non-small cell lung cancer"], not working
  ["hsa01521", "EGFR tyrosine kinase inhibitor resistance"],
  ["hsa01524", "Platinum drug resistance"],
  ["hsa01523", "Antifolate resistance"],
  //["hsa01522", "Endocrine resistance"], not working
  //["hsa07040", "Antineoplastics - alkylating agents"], not working
  //["hsa07041", "Antineoplastics - antimetabolic agents"], not working
  //["hsa07042", "Antineoplastics - agents from natural products"], not working
  //["hsa07043", "Antineoplastics - hormones"], not working
  //["hsa07045", "Antineoplastics - protein kinase inhibitors"] not working
  ]

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      selected_pathway: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.selected_pathway);

    var selected_pathway: string = this.ccsForm.value.selected_pathway;

    var req: string = this.buildRequest(selected_pathway);

    let img = document.getElementById('iframeID');
    console.log(typeof (img));
    if (img instanceof HTMLIFrameElement) img.src = req;
  }

  resetLastSearch() {
    this.liftover = new LiftOver();

    let img = document.getElementById('iframeID');
    console.log(typeof (img));
    if (img instanceof HTMLIFrameElement) img.src = "";
  }

  buildRequest(selected_pathway: string) {
    var request: string = this.requestSrc + "/"
    request += selected_pathway;
    console.log(request);
    return request;
  }
}

class Data {
  html: string;

  constructor(html: string) {
    this.html = html
  }
}

class LiftOver {
  data: Array<Data> = [];
  number_elements = 0;

  fill_data(html: any) {
    this.data.push(new Data(html));
  }
}


