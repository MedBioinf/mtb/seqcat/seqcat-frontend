import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterFusionCheckComponent } from './converter-fusion-check.component';

describe('ConverterFusionCheckComponent', () => {
  let component: ConverterFusionCheckComponent;
  let fixture: ComponentFixture<ConverterFusionCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterFusionCheckComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterFusionCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
