import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-fusion-check',
  templateUrl: './converter-fusion-check.component.html',
  styleUrls: ['./converter-fusion-check.component.css']
})
export class ConverterFusionCheckComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/fusionIsInFrame/hg38/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);

    var inputName: string = this.ccsForm.value.inputName;

    var req: string = this.buildRequest(inputName);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string) {
    var request: string = this.requestSrc + "/"
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['chr15:75688603-chr11:6622666,chr7:140753336-chr6:17799397']

    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;

    this.ccsForm.patchValue({
      inputName: example,
    });
  }
}

class Data {
  inframe: string;

  i1_gene_name: string;
  i1_position_in_frame: string;
  i1_position_in_triplett: string;
  i1_qid: string;
  i1_strand: string;
  i1_target_exon: string;
  i1_transcript: string;

  i2_gene_name: string;
  i2_position_in_frame: string;
  i2_position_in_triplett: string;
  i2_qid: string;
  i2_strand: string;
  i2_target_exon: string;
  i2_transcript: string;

  name: string;
  response: string;
  error: string;


  constructor(name: string, response: string, error: string,
    inframe: string,

    i1_gene_name: string,
    i1_position_in_frame: string,
    i1_position_in_triplett: string,
    i1_qid: string,
    i1_strand: string,
    i1_target_exon: string,
    i1_transcript: string,

    i2_gene_name: string,
    i2_position_in_frame: string,
    i2_position_in_triplett: string,
    i2_qid: string,
    i2_strand: string,
    i2_target_exon: string,
    i2_transcript: string) {

    this.response = response
    this.name = name;
    this.error = error;

    this.inframe = inframe;
    this.i1_gene_name = i1_gene_name;
    this.i1_position_in_frame = i1_position_in_frame;
    this.i1_position_in_triplett = i1_position_in_triplett;
    this.i1_qid = i1_qid;
    this.i1_strand = i1_strand;
    this.i1_target_exon = i1_target_exon;
    this.i1_transcript = i1_transcript;

    this.i2_gene_name = i2_gene_name;
    this.i2_position_in_frame = i2_position_in_frame;
    this.i2_position_in_triplett = i2_position_in_triplett;
    this.i2_qid = i2_qid;
    this.i2_strand = i2_strand;
    this.i2_target_exon = i2_target_exon;
    this.i2_transcript = i2_transcript;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      if (j_element['header'] != null) {
        if (j_element['header']['status_code'] == 200) {
          this.data.push(
            new Data(
              j_element['header']['qid'],
              j_element['header']['status_code'],
              "",
              j_element['data']['is_inframe'],
              j_element['data']['input_1']['gene_name'],
              j_element['data']['input_1']['position_in_frame'],
              j_element['data']['input_1']['position_in_triplett'],
              j_element['data']['input_1']['qid'],
              j_element['data']['input_1']['strand'],
              j_element['data']['input_1']['target_exon'],
              j_element['data']['input_1']['transcript'],
              j_element['data']['input_2']['gene_name'],
              j_element['data']['input_2']['position_in_frame'],
              j_element['data']['input_2']['position_in_triplett'],
              j_element['data']['input_2']['qid'],
              j_element['data']['input_2']['strand'],
              j_element['data']['input_2']['target_exon'],
              j_element['data']['input_2']['transcript']));
          this.number_elements += 1;
        }
        else {
          this.data.push(
            new Data(
              j_element['header']['qid'],
              j_element['header']['status_code'],
              j_element['header']['error_message'],
              "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""));
        }
      }
    }
  }
}

