import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-converter-liftover',
  templateUrl: './converter-liftover.component.html',
  styleUrls: ['./converter-liftover.component.css']
})
export class ConverterLiftoverComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  same_reference_genomes: boolean = false;

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/liftover/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenomeFrom: ['', { validators: [Validators.required] }],
      referenceGenomeTo: ['', { validators: [Validators.required] }],
    })
  }


  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenomeFrom);
    console.log(this.ccsForm.value.referenceGenomeTo);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenomeFrom: string = this.ccsForm.value.referenceGenomeFrom;
    var referenceGenomeTo: string = this.ccsForm.value.referenceGenomeTo;

    this.reqObj = new LiftOver()

    if (referenceGenomeFrom == referenceGenomeTo) {
      this.same_reference_genomes = true;
    }
    else {
      var req: string = this.buildRequest(inputName, referenceGenomeFrom, referenceGenomeTo);
      this.complete_request = req;

      this.same_reference_genomes = false;
      this.http.get(req).subscribe((json: any) => {
        console.log(json);
        this.reqObj.fill_data(json);
        console.log(this.reqObj);
      });
    }
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string, referenceGenomeFrom: string, referenceGenomeTo: string) {
    var request: string = this.requestSrc + referenceGenomeFrom + ':' + referenceGenomeTo + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['chr1:10000,chr1:623876,chr1:100,chr3:56326424,chr5:324234,chr17:7673776,chr17:7673777,chr17:7673778']
    var example: string = examples[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById("hg38_f")).checked = true;
    (<HTMLInputElement>document.getElementById("t2t_t")).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenomeFrom: 'hg38',
      referenceGenomeTo: 't2t',
    });
  }
}

class Data {
  name: string;
  chromosome: string;
  new_position: string;
  strand: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, chrom: string, pos: string, strand: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.chromosome = chrom;
    this.new_position = pos;
    this.strand = strand;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['chromosome'],
            j_element['data']['position'],
            j_element['data']['strand']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          "",
          "",
          ""));
        }
      }
    }
  }
}

