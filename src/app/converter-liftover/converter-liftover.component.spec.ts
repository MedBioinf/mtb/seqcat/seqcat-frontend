import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterLiftoverComponent } from './converter-liftover.component';

describe('ConverterLiftoverComponent', () => {
  let component: ConverterLiftoverComponent;
  let fixture: ComponentFixture<ConverterLiftoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterLiftoverComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterLiftoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
