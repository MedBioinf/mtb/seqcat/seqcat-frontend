import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterDnaInfoComponent } from './converter-dna-info.component';

describe('ConverterDnaInfoComponent', () => {
  let component: ConverterDnaInfoComponent;
  let fixture: ComponentFixture<ConverterDnaInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterDnaInfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterDnaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
