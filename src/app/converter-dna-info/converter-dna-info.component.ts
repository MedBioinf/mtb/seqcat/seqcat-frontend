import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-converter-dna-info',
  templateUrl: './converter-dna-info.component.html',
  styleUrls: ['./converter-dna-info.component.css']
})
export class ConverterDnaInfoComponent {

  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();

  random_iterator: number = 0

  reqObj: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de//CCS/v1/GetSequence/';
  complete_request: string = "";

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);
    this.complete_request = req;
    this.reqObj = new LiftOver()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  getExampleData() {
    var examples: Array<string> = ['chr1:50200-50400,chr1:50800-50400,chr11:89200-89500,chr15:27445621-27445671']
    var references: Array<string> = ['hg38']

    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Data {
  name: string;
  response: string;
  error: string;
  sequence: string;

  constructor(name: string, response: string, error: string, sequence: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.sequence = sequence;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['sequence']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          ""));
        }
      }
    }
  }
}