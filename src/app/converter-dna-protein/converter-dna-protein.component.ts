import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-converter-dna-protein',
  templateUrl: './converter-dna-protein.component.html',
  styleUrls: ['./converter-dna-protein.component.css']
})



export class ConverterDnaProteinComponent {
  ccsForm: FormGroup;

  liftover: LiftOver = new LiftOver();
  reqVCF: VCF = new VCF();

  random_iterator: number = 0

  reqObj: any;
  reqObjVcf: any;
  selReqObj: string = "";

  requestSrc: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/GenomicToGene/';
  complete_request: string = "";
  requestSrcVCF: string = 'https://mtb.bioinf.med.uni-goettingen.de/CCS/v1/GenomicToGeneVCF/';

  constructor(fb: FormBuilder, private http: HttpClient) {
    this.ccsForm = fb.group({
      inputName: ['', { validators: [Validators.required] }],
      referenceGenome: ['', { validators: [Validators.required] }],
    })
  }

  markCode(test:any) {
    test.select();
  }

  getDrugInfo() {
    this.resetLastSearch();

    console.log(this.ccsForm.value.inputName);
    console.log(this.ccsForm.value.referenceGenome);

    var inputName: string = this.ccsForm.value.inputName;
    var referenceGenome: string = this.ccsForm.value.referenceGenome;

    var req: string = this.buildRequest(inputName, referenceGenome);
    this.complete_request = req;
    var reqVCF : string = this.buildRequestVCF(inputName, referenceGenome);
    this.reqObj = new LiftOver()
    this.reqObjVcf = new VCF()

    this.http.get(req).subscribe((json: any) => {
      console.log(json);
      this.reqObj.fill_data(json);
      console.log(this.reqObj);
    });

    this.http.get(reqVCF).subscribe((json: any) => {
      console.log(json);
      this.reqObjVcf.fill_data(json);
      console.log(this.reqObjVcf);
    });
  }

  resetLastSearch() {
    this.liftover = new LiftOver();
    this.reqVCF = new VCF();
  }

  buildRequest(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrc + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }

  buildRequestVCF(inputName: string, referenceGenome: string) {
    var request: string = this.requestSrcVCF + referenceGenome + '/';
    request += inputName;
    console.log(request);
    return request;
  }


  getExampleData() {
    var examples: Array<string> = ['chr17:g.7673776G>A,chr17:g.7673776C>A,chr11:g.6622666G>A,chr15:g.75688603A>G,chr2:g.21006635G>T,chr3:g.169381339G>T,chr6:g.158980908A>C,chr6:17799397T>C,chr7:g.140753336A>T']
    var references: Array<string> = ['hg38']


    var example: string = examples[this.random_iterator];
    var reference: string = references[this.random_iterator];

    if (this.random_iterator == examples.length - 1) {
      this.random_iterator = 0;
    } else {
      this.random_iterator += 1;
    }

    (<HTMLInputElement>document.getElementById("inputName")).value = example;
    (<HTMLInputElement>document.getElementById(reference)).checked = true;

    this.ccsForm.patchValue({
      inputName: example,
      referenceGenome: reference,
    });
  }
}

class Data {
  name: string;
  transcript: string;
  variant: string;
  exchange: string;
  gene_name: string;
  response: string;
  error: string;

  constructor(name: string, response: string, error: string, transcript: string, variant: string, variant_exchange: string, gene_name: string) {
    this.response = response
    this.name = name;
    this.error = error;
    this.transcript = transcript;
    this.variant = variant;
    this.exchange = variant_exchange;
    this.gene_name = gene_name;
  }
}

class LiftOver {

  data: Array<Data> = [];
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    for (let j_element of json) {
      // Check 
      if (j_element['header'] != null) {
        if(j_element['header']['status_code'] == 200) {
          this.data.push(new Data(j_element['header']['qid'],
            j_element['header']['status_code'],
            j_element['header']['error_message'],
            j_element['data']['transcript'],
            j_element['data']['variant'],
            j_element['data']['variant_exchange'],
            j_element['data']['gene_name']));
          this.number_elements += 1;
        }
        else {
          this.data.push(new Data(j_element['header']['qid'],
          j_element['header']['status_code'],
          j_element['header']['error_message'],
          "",
          "",
          "",
          ""));
        }
      }
    }
  }
}

class VCF {

  data: string = "";
  number_elements = 0;

  fill_data(json: any) {
    if (json.length == 0) {
      return;
    }
    this.data = json['vcf'];
    this.data.replace('\n', '<br>')
  }
}