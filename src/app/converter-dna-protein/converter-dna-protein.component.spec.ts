import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConverterDnaProteinComponent } from './converter-dna-protein.component';

describe('ConverterDnaProteinComponent', () => {
  let component: ConverterDnaProteinComponent;
  let fixture: ComponentFixture<ConverterDnaProteinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConverterDnaProteinComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConverterDnaProteinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
