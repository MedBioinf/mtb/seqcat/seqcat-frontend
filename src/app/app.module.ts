import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxMatomoTrackerModule } from '@ngx-matomo/tracker';
import { NgxMatomoRouterModule } from '@ngx-matomo/router';


import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';

import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ConverterComponent } from './converter/converter.component';
import { HomeComponent } from './home/home.component';
import { ApiComponent } from './api/api.component';
import { AboutComponent } from './about/about.component';
import { ConverterLiftoverComponent } from './converter-liftover/converter-liftover.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConverterDnaProteinComponent } from './converter-dna-protein/converter-dna-protein.component';
import { ConverterProteinDnaComponent } from './converter-protein-dna/converter-protein-dna.component';
import { ConverterTranscriptsComponent } from './converter-transcripts/converter-transcripts.component';
import { ConverterProteinInfoComponent } from './converter-protein-info/converter-protein-info.component';
import { ConverterAminoInfoComponent } from './converter-amino-info/converter-amino-info.component';
import { ConverterAaTableComponent } from './converter-aa-table/converter-aa-table.component';
import { ConverterReverseComplementComponent } from './converter-reverse-complement/converter-reverse-complement.component';
import { ConverterBlossumMatrixComponent } from './converter-blossum-matrix/converter-blossum-matrix.component';
import { ConverterBlossumMatrixScoreCalculatorComponent } from './converter-blossum-matrix-score-calculator/converter-blossum-matrix-score-calculator.component';
import { ConverterGenomToProteinSequenceComponent } from './converter-genom-to-protein-sequence/converter-genom-to-protein-sequence.component';
import { ConverterNcbiEnsmblTranscriptsComponent } from './converter-ncbi-ensmbl-transcripts/converter-ncbi-ensmbl-transcripts.component';
import { ConverterGeneInKeggPathwayComponent } from './converter-gene-in-kegg-pathway/converter-gene-in-kegg-pathway.component';
import { ConverterFusionCheckComponent } from './converter-fusion-check/converter-fusion-check.component';
import { ConverterGeneNormalizerComponent } from './converter-gene-normalizer/converter-gene-normalizer.component';
import { HelpingComponent } from './helping/helping.component';
import { ConverterExonInfoComponent } from './converter-exon-info/converter-exon-info.component';
import { ConverterDnaInfoComponent } from './converter-dna-info/converter-dna-info.component';
import { TerminalViewComponent } from './terminal-view/terminal-view.component';
import { InputFormViewComponent } from './input-form-view/input-form-view.component';
import { ConverterTranscriptToGeneGenomicComponent } from './converter-transcript-to-gene-genomic/converter-transcript-to-gene-genomic.component';
import { ConverterTranscriptToGeneidComponent } from './converter-transcript-to-geneid/converter-transcript-to-geneid.component';
import { ImpressumDataPrivacyComponent } from './impressum-data-privacy/impressum-data-privacy.component';
import { ImprintComponent } from './imprint/imprint.component';
import { DataPrivacyComponent } from './data-privacy/data-privacy.component';

@NgModule({
  declarations: [
    AppComponent,
    ConverterComponent,
    HomeComponent,
    ApiComponent,
    AboutComponent,
    ConverterLiftoverComponent,
    ConverterDnaProteinComponent,
    ConverterProteinDnaComponent,
    ConverterTranscriptsComponent,
    ConverterProteinInfoComponent,
    ConverterAminoInfoComponent,
    ConverterAaTableComponent,
    ConverterReverseComplementComponent,
    ConverterBlossumMatrixComponent,
    ConverterBlossumMatrixScoreCalculatorComponent,
    ConverterGenomToProteinSequenceComponent,
    ConverterNcbiEnsmblTranscriptsComponent,
    ConverterGeneInKeggPathwayComponent,
    ConverterFusionCheckComponent,
    ConverterGeneNormalizerComponent,
    HelpingComponent,
    ConverterExonInfoComponent,
    ConverterDnaInfoComponent,
    TerminalViewComponent,
    InputFormViewComponent,
    ConverterTranscriptToGeneGenomicComponent,
    ConverterTranscriptToGeneidComponent,
    ImpressumDataPrivacyComponent,
    ImprintComponent,
    DataPrivacyComponent
  ],
  imports: [
    BrowserModule,
    NgxMatomoTrackerModule.forRoot({
      siteId: '9', // your Matomo's site ID (find it in your Matomo's settings)
      trackerUrl: 'https://mtb.bioinf.med.uni-goettingen.de/matomo', // your matomo server root url
    }),
    NgxMatomoRouterModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    MatIconModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
