import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ConverterComponent } from './converter/converter.component';
import { ApiComponent } from './api/api.component';
import { AboutComponent } from './about/about.component';
import { ConverterLiftoverComponent } from './converter-liftover/converter-liftover.component';
import { ConverterDnaProteinComponent } from './converter-dna-protein/converter-dna-protein.component';
import { ConverterProteinDnaComponent } from './converter-protein-dna/converter-protein-dna.component';
import { ConverterTranscriptsComponent } from './converter-transcripts/converter-transcripts.component';
import { ConverterProteinInfoComponent } from './converter-protein-info/converter-protein-info.component';
import { ConverterAminoInfoComponent } from './converter-amino-info/converter-amino-info.component';
import { ConverterAaTableComponent } from './converter-aa-table/converter-aa-table.component';
import { ConverterReverseComplementComponent } from './converter-reverse-complement/converter-reverse-complement.component';
import { ConverterBlossumMatrixComponent } from './converter-blossum-matrix/converter-blossum-matrix.component';
import { ConverterBlossumMatrixScoreCalculatorComponent } from './converter-blossum-matrix-score-calculator/converter-blossum-matrix-score-calculator.component';
import { ConverterGenomToProteinSequenceComponent } from './converter-genom-to-protein-sequence/converter-genom-to-protein-sequence.component';
import { ConverterNcbiEnsmblTranscriptsComponent } from './converter-ncbi-ensmbl-transcripts/converter-ncbi-ensmbl-transcripts.component';
import { ConverterGeneInKeggPathwayComponent } from './converter-gene-in-kegg-pathway/converter-gene-in-kegg-pathway.component';
import { ConverterFusionCheckComponent } from './converter-fusion-check/converter-fusion-check.component';
import { ConverterGeneNormalizerComponent } from './converter-gene-normalizer/converter-gene-normalizer.component';
import { ConverterExonInfoComponent } from './converter-exon-info/converter-exon-info.component';
import { ConverterDnaInfoComponent } from './converter-dna-info/converter-dna-info.component';
import { ConverterTranscriptToGeneidComponent } from './converter-transcript-to-geneid/converter-transcript-to-geneid.component';
import { ConverterTranscriptToGeneGenomicComponent } from './converter-transcript-to-gene-genomic/converter-transcript-to-gene-genomic.component';
import { HelpingComponent } from './helping/helping.component';
import { ImprintComponent } from './imprint/imprint.component';
import { ImpressumDataPrivacyComponent } from './impressum-data-privacy/impressum-data-privacy.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'converter', component: ConverterComponent },
  { path: 'api', component: ApiComponent },
  { path: 'about', component: AboutComponent },
  { path: 'converter/converter-liftover', component: ConverterLiftoverComponent },
  { path: 'converter/converter-dna-protein', component: ConverterDnaProteinComponent },
  { path: 'converter/converter-protein-dna', component: ConverterProteinDnaComponent },
  { path: 'converter/converter-transcript', component: ConverterTranscriptsComponent },
  { path: 'converter/converter-protein-info', component: ConverterProteinInfoComponent },
  { path: 'converter/converter-amino-info', component: ConverterAminoInfoComponent },
  { path: 'converter/converter-aa-table', component: ConverterAaTableComponent },
  { path: 'converter/converter-blossum-matrix', component: ConverterBlossumMatrixComponent },
  { path: 'converter/converter-reverse-complement', component: ConverterReverseComplementComponent },
  { path: 'converter/converter-blossum-matrix-score-calculator', component: ConverterBlossumMatrixScoreCalculatorComponent },
  { path: 'converter/converter-genome-to-protein-sequence', component: ConverterGenomToProteinSequenceComponent },
  { path: 'converter/converter-ncbi-ensmbl-transcripts', component: ConverterNcbiEnsmblTranscriptsComponent },
  { path: 'converter/converter-gene-in-kegg-pathway', component: ConverterGeneInKeggPathwayComponent },
  { path: 'converter/converter-fusion-check', component: ConverterFusionCheckComponent },
  { path: 'converter/converter-gene-normalizer', component: ConverterGeneNormalizerComponent },
  { path: 'converter/converter-exon-info', component: ConverterExonInfoComponent},
  { path: 'converter/converter-dna-info', component: ConverterDnaInfoComponent},
  { path: 'converter/converter-transcript-to-geneid', component: ConverterTranscriptToGeneidComponent},
  { path: 'converter/converter-transcript-to-gene-genomic', component: ConverterTranscriptToGeneGenomicComponent},
  { path: 'imprint', component: ImprintComponent },
  { path: 'impressum-data-privacy', component: ImpressumDataPrivacyComponent },
  { path: 'help', component: HelpingComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
